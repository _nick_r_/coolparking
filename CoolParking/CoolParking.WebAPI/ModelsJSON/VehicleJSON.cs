using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace CoolParking.WebAPI.ModelsJSON
{
	public class VehicleJSON
	{
		[JsonProperty("id")]
		[Required]
		public string Id { get; set; }
		[JsonProperty("vehicleType")]
		[Required]
		public VehicleType VehicleType { get; set;  }
		[JsonProperty("balance")]
		[Required]
		public decimal Balance { get; set; }
	}
}