using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace CoolParking.WebAPI.ModelsJSON
{
	public class TopUpVehicleJSON
	{
		[JsonProperty("id")]
		[Required]
		public string Id { get; set; }
		[JsonProperty("Sum")]
		[Required]
		public decimal Sum { get; set; }
	}
}