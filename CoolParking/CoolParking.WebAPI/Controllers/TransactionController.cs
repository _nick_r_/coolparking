using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Services;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Text.RegularExpressions;
using CoolParking.WebAPI.ModelsJSON;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TransactionsController : ControllerBase
    {
        private IParkingService Service;
        public TransactionsController(IParkingService service)
        {
            Service = service;
        }

        [HttpGet]
        [Route("last")]
        public ActionResult<IReadOnlyCollection<TransactionInfo>> GetLatsTransactions()
        {
            return Ok(Service.GetLastParkingTransactions());
        }

        [HttpGet]
        [Route("all")]
        public ActionResult<string> AllTransactions()
        {
            if (!System.IO.File.Exists(Settings.logPath))
                return NotFound("File is not found");
            return Ok(Service.ReadFromLog());
        }

        [HttpPut]
        [Route("topUpVehicle")]
        public ActionResult<Vehicle> TopUpVehicle(TopUpVehicleJSON topUpVehicle)
        {
            if ((topUpVehicle.Sum <= 0) || !Vehicle.IdValid(topUpVehicle.Id))
                return BadRequest();
            Vehicle vehicle = Parking.GetInstance().GetVehicle(topUpVehicle.Id);
            if (vehicle == null)
                return NotFound();
            Service.TopUpVehicle(topUpVehicle.Id, topUpVehicle.Sum);
            return Ok(vehicle);
        }
    }
}