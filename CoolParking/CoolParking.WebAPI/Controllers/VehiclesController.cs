﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using CoolParking.BL.Models;
using CoolParking.BL.Interfaces;
using System.Text.RegularExpressions;
using CoolParking.WebAPI.ModelsJSON;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private IParkingService Service { get; set; }
        public VehiclesController(IParkingService service)
        {
            Service = service;
        }

        [HttpGet]
        public ActionResult<IReadOnlyCollection<Vehicle>> GetVehiclesCollection() => Ok(Service.GetVehicles());


        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehiclesById(string id)
        {
            Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            if (!regex.IsMatch(id))
                return BadRequest("Invalid id");
            Vehicle vehicle = Parking.GetInstance().GetVehicle(id);
            if (vehicle == null)
                return NotFound("There is no vehicles with this id");
            return vehicle;
        }

        [HttpPost]
        public ActionResult<Vehicle> AddVehicle([FromBody]VehicleJSON veh)
        {
            if (!((veh.VehicleType > 0 && (int)veh.VehicleType < 5) && (veh.Balance > 0 && veh.Balance <= decimal.MaxValue) && Vehicle.IdValid(veh.Id)))
			{
				return BadRequest();
			}
            Vehicle vehicle;
            try
            {
                vehicle = new Vehicle(veh.Id, veh.VehicleType, veh.Balance);
                Service.AddVehicle(vehicle);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Created("./CoolParking/Models/Parking.Vehicles", vehicle);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicles(string id)
        {
            Regex regex = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            if (!regex.IsMatch(id))
                return BadRequest("Invalid id");
            Vehicle vehicle = Parking.GetInstance().GetVehicle(id);
            if (vehicle == null)
                return NotFound("There is no vehicles with this id");
            Service.RemoveVehicle(id);
            return NoContent();
        }
    }
}