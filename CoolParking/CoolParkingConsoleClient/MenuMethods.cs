﻿using CoolParkingConsoleClient.ClientModels;
using CoolParkingConsoleClient.ClientServices;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParkingConsoleClient
{
    public static class MenuMethods
    {
        public static void ShowMenu()
        {
            Console.WriteLine("Choose a number:\n" +
               "1--Show parking balance\n" +
               "2--Show parking capacity\n" +
               "3--Show amount of parking free places\n" +
               "4--Show history of transactions for current period\n" +
               "5--Show list of vehicles which on the parking now\n" +
               "6--Add vehicle on parking\n" +
               "7--Remove a vehicle from the parking\n" +
               "8--Top up balance of a specific vehicle\n" +
               "9--Get a vehicle by id\n" +
               "10--Read information from log file\n" +               
               "11--Exit\n");
        }


        public static void MainMethod()
        {

            string number = Console.ReadLine();
            try
            {
                MenuCases(number);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"Mistake:{ex.Message}");
            }

            catch (InvalidOperationException ex)
            {
                Console.WriteLine($"Mistake:{ex.Message}");
            }
            catch (FormatException ex)
            {
                Console.WriteLine($"Mistake:{ex.Message}");
            }
            catch (AggregateException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Mistake:{ex.Message}");
            }
            finally
            {
                ShowMenu();
                MainMethod();
            }
        }

        private static void MenuCases(string number)
        {
            switch (number)
            {
                case "1":
                    Console.WriteLine($"Current balance is {ParkingClientHTTPService.GetParkingHttp("balance").Result}\n");
                    break;
                case "2":
                    Console.WriteLine($"The capacity of the parking is {ParkingClientHTTPService.GetParkingHttp("capacity").Result}\n");
                    break;
                case "3":
                    Console.WriteLine($"Amount of free places of the parking is {ParkingClientHTTPService.GetParkingHttp("freePlaces").Result}");
                    break;
                case "4":
                    Console.WriteLine($"The history of transactions for current period is {GetNormalStringOfTransactions()}");
                    break;
                case "5":
                    GetVehiclesMenu();
                    break;
                case "6":
                    AddVehicleMenu();
                    break;
                case "7":
                    RemoveVehicleMenu();
                    break;
                case "8":
                    TopUpVehicleMenu();
                    break;
                case "9":
                    GetVehicleMenu();
                    break;
                case "10":
                    Console.WriteLine(TransactionsClientHTTPService.GetAllTransactions().Result);
                    break;                
                case "11":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Invalid number");
                    break;
            }
        }
        private static void GetVehiclesMenu()
        {
            ReadOnlyCollection<VehicleClient> clients = new ReadOnlyCollection<VehicleClient>(VehiclesClientHTTPService.GetVehicles().Result);
            for (int i = 0; i < clients.Count; i++)
                Console.Write(clients[i].Id + " with balance " + clients[i].Balance + "\n");
        }

        private static void AddVehicleMenu()
        {
            Console.WriteLine("Enter transport id (XX-0000-XX)");
            string id = Console.ReadLine();
            Console.WriteLine("Enter transport type (1 - Bus, 2 - PassengerCar, 3 - Truck and 4 - Motorcycle)");
            int type = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter transport balance");
            decimal money = Convert.ToDecimal(Console.ReadLine());
            VehicleClient vehicle = new VehicleClient(id, type, money);
            string rez = VehiclesClientHTTPService.AddVehicle(vehicle).Result;
            Console.WriteLine($"{rez}\n");
        }

        private static void RemoveVehicleMenu()
        {
            Console.WriteLine("Enter the transport id:");
            string idRemove = Console.ReadLine();
            string rezRev = VehiclesClientHTTPService.RemoveVehicle(idRemove).Result;
            Console.WriteLine(rezRev + "\n");
        }

        private static void TopUpVehicleMenu()
        {
            Console.WriteLine("Enter sum and transport id:");
            decimal sum = Convert.ToDecimal(Console.ReadLine());
            string ids = Console.ReadLine();
            VehicleClient rezTopUp = TransactionsClientHTTPService.TopUpVehicle(ids, sum).Result;
            Console.WriteLine($"Now {rezTopUp.Id} is staying on parking with balance {rezTopUp.Balance}\n");
        }
        private static void GetVehicleMenu()
        {
            Console.WriteLine("Enter the transport id:");
            string IdTr = Console.ReadLine();
            VehicleClient rezGet = VehiclesClientHTTPService.GetVehicleById(IdTr).Result;
            Console.WriteLine($"Vehicle with id {rezGet.Id} with balance {rezGet.Balance} is staying now on the parking\n");
        }

        static string GetNormalStringOfTransactions()
        {
            string rez = String.Empty;
            TransactionClient[] transactionInfos = TransactionsClientHTTPService.GetLastTransactions().Result;
            for (int i = 0; i < transactionInfos.Length; i++)
            {
                rez += $"{transactionInfos[i].TimeTransaction:T} -- {transactionInfos[i].VehicleId}  {transactionInfos[i].Sum}\n";
            }
            return rez;
        }
    }
}
