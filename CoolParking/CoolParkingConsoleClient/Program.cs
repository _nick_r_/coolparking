﻿using CoolParkingConsoleClient.ClientModels;
using System;
using System.Collections.ObjectModel;
using CoolParkingConsoleClient.ClientServices;
using CoolParkingConsoleClient;



namespace CoolParkingConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            MenuMethods.ShowMenu();
            MenuMethods.MainMethod();

        }
       
    }
}
