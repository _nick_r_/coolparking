﻿using CoolParkingConsoleClient.ClientModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoolParkingConsoleClient.ClientServices
{
    public static class VehiclesClientHTTPService
    {
        private static HttpClient Client { get; set; } = new HttpClient();

        private static string BaseUrl = new string(@"https://localhost:5001/api/");

        public static async Task<IList<VehicleClient>> GetVehicles()
        {
            var resp = await Client.GetAsync(BaseUrl + $"vehicles");
            return JsonSerializer.Deserialize<IList<VehicleClient>>(resp.Content.ReadAsStringAsync().Result);
        }

        public static async Task<VehicleClient> GetVehicleById(string id)
        {
            var resp = await Client.GetAsync(BaseUrl + $"vehicles/{id}");
            if (resp.StatusCode == HttpStatusCode.NotFound || resp.StatusCode == HttpStatusCode.BadRequest)//404 and 400 code 
                throw new ArgumentException("Id have incorrect format or could not found this id");
            string vehicle = await resp.Content.ReadAsStringAsync();
            return JsonSerializer.Deserialize<VehicleClient>(vehicle);
        }

        public static async Task<string> AddVehicle(VehicleClient vehicle)
        {
            var resp = await Client.SendAsync(new HttpRequestMessage(HttpMethod.Post, BaseUrl +
                $"vehicles?id={vehicle.Id}&vehicleType={vehicle.VehicleType}&balance={vehicle.Balance}"));
            if (resp.StatusCode == HttpStatusCode.BadRequest)//400
                throw new ArgumentException("This vehicle is already on parking or invalid body");
            return "This vehicle was successfully added:" + await resp.Content.ReadAsStringAsync();
        }

        public static async Task<string> RemoveVehicle(string id)
        {
            var resp = await Client.DeleteAsync(BaseUrl + $"vehicles/{id}");
            if (resp.StatusCode == HttpStatusCode.NotFound || resp.StatusCode == HttpStatusCode.BadRequest)//404 and 400
                throw new ArgumentException("Invalid id or  there is no vehicle with this id");
            return "Successfully deleted:" + resp.StatusCode;
        }

    }
}
