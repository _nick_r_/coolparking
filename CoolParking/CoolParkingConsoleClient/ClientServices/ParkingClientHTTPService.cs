﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParkingConsoleClient.ClientServices
{
    public static class ParkingClientHTTPService
    {
        private static HttpClient Client { get; set; } = new HttpClient();

        private static string BaseUrl = new string(@"https://localhost:5001/api/");


        public static async Task<string> GetParkingHttp(string LastEndpoint)
        {
            var resp = await Client.GetAsync(BaseUrl + $"parking/{LastEndpoint}");
            return await resp.Content.ReadAsStringAsync();
        }
    }
}
