﻿using CoolParkingConsoleClient.ClientModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoolParkingConsoleClient.ClientServices
{
    public static class TransactionsClientHTTPService
    {
        private static HttpClient Client { get; set; } = new HttpClient();

        private static string BaseUrl = new string(@"https://localhost:5001/api/");

        public static async Task<TransactionClient[]> GetLastTransactions()
        {
            var resp = await Client.GetAsync(BaseUrl + "transactions/last");
            Console.WriteLine(resp.Content.ReadAsStringAsync().Result);
            return JsonSerializer.Deserialize<TransactionClient[]>(await resp.Content.ReadAsStringAsync());
        }

        public static async Task<string> GetAllTransactions()
        {
            var resp = await Client.GetAsync(BaseUrl + "transactions/all");
            if (resp.StatusCode == HttpStatusCode.NotFound)//404
                throw new ArgumentException("File not found");
            return await resp.Content.ReadAsStringAsync();
        }

        public static async Task<VehicleClient> TopUpVehicle(string id, decimal sum)
        {

            var resp = await Client.SendAsync(new HttpRequestMessage(HttpMethod.Put, BaseUrl + $"transactions/topUpVehicle?id={id}&sum={sum}"));
            if (resp.StatusCode == HttpStatusCode.NotFound || resp.StatusCode == HttpStatusCode.BadRequest)// 400 and 404
                throw new ArgumentException("Id not found or sum less than zero");
            return JsonSerializer.Deserialize<VehicleClient>(await resp.Content.ReadAsStringAsync());
        }

    }
}
