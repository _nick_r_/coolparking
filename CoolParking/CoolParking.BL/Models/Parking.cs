﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.


using System.Collections.Generic;

namespace CoolParking.BL.Models
{


    public class Parking
    {
        private static Parking instanse;

        public decimal balance;
        public List<Vehicle> VehicleCollection;

        private Parking()
        {
            balance = 0;
            VehicleCollection = new List<Vehicle>();
        }

        public static Parking GetInstance()
        {
            return instanse ??= new Parking();
        }

        public Vehicle GetVehicle(string id)
        {         
            return VehicleCollection.Find((Vehicle x) => x.Id == id);
        }

    }
}