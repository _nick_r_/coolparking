﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Text;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;


namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        [JsonPropertyName("id")]
        public string Id { get; }

        [JsonPropertyName("vehicleType")]
        public VehicleType VehicleType { get; }

        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }




        public Vehicle(string id, VehicleType type, decimal balance)
        {
            Regex reg = new Regex("^[A-Z]{2}[-][0-9]{4}[-][A-Z]{2}$");
            string pattern = "^[A-Z]{2}[-][0-9]{4}[-][A-Z]{2}$";

            if ((!Regex.IsMatch(id, pattern)) || (balance < 0))
            {
                throw new ArgumentException();

            }

            Id = id;
            VehicleType = type;
            Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {

            string letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            return letters[new Random().Next(0, 26)].ToString() + letters[new Random().Next(0, 26)].ToString()
                + "-"
                + new Random().Next(0, 10) + new Random().Next(0, 10)
                + new Random().Next(0, 10) + new Random().Next(0, 10)
                + "-"
                + letters[new Random().Next(0, 26)].ToString() + letters[new Random().Next(0, 26)].ToString();
        }



        public static bool IdValid(string id)
        {
            Regex regex = new Regex(@"^[A-Z]{2}-\d{4}-[A-Z]{2}$");
            if (regex.IsMatch(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}