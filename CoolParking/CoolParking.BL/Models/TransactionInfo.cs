﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.


using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public string TimeOfTranaction { get; set; }
        public string Id { get; set; }
        public decimal Sum { get; set; }


        public TransactionInfo(string time, string id, decimal sum)
        {
            TimeOfTranaction = time;
            Id = id;
            Sum = sum;
        }

        public override string ToString()
        {
            string result = "";
            result = "Vehicle ID: " + Id + "; Sum: " + Sum.ToString() + "; Time of transaction: " + TimeOfTranaction;

            return result;
        }
    }
}