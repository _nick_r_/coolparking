﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static readonly int initialBalance = 0;
        public static readonly int capacity = 10;
        public static readonly int payPeriod = 5;
        public static readonly int logPeriod = 60;

        public static readonly decimal PassengerCar = 2;
        public static readonly decimal Truck = 5;
        public static readonly decimal Bus = 3.5m;
        public static readonly decimal Motorcycle = 1;

        public static readonly decimal fine = 2.5m;
        public static readonly string logPath = ".\\Transactions.log";
    }
}