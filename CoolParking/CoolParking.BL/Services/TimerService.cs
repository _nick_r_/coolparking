﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;

using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private System.Timers.Timer aTimer;
        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }

        public void Start()
        {
            aTimer = new Timer(Interval);


            aTimer.Elapsed += Elapsed;
            aTimer.AutoReset = true;
            aTimer.Enabled = true;

        }

        public void Stop()
        {
            aTimer.Stop();
        }

        public void Dispose()
        {

        }



    }
}