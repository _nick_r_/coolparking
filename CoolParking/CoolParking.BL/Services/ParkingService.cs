﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;
using CoolParking.BL.Models;
using CoolParking.BL.Services;


namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking parking;
        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;
        private List<TransactionInfo> transactionInfo;

        public ParkingService()
        {
            parking = Parking.GetInstance();
            withdrawTimer = new TimerService();
            logTimer = new TimerService();
            logService = new LogService("Transactions.log");

            transactionInfo = new List<TransactionInfo>();

            withdrawTimer.Interval = 5000;
            logTimer.Interval = 60000;

            withdrawTimer.Elapsed += MakeAPayment;
            logTimer.Elapsed += Write;

            withdrawTimer.Start();
            logTimer.Start();
        }
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            parking = Parking.GetInstance();
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;

            transactionInfo = new List<TransactionInfo>();

            withdrawTimer.Interval = 5000;
            logTimer.Interval = 60000;

            withdrawTimer.Elapsed += MakeAPayment;
            logTimer.Elapsed += Write;

            withdrawTimer.Start();
            logTimer.Start();
        }



        private void MakeAPayment(object sender, ElapsedEventArgs e)
        {


            for (int i = 0; i < parking.VehicleCollection.Count; i++)
            {
                switch (parking.VehicleCollection[i].VehicleType)
                {
                    case VehicleType.PassengerCar:
                        {
                            parking.VehicleCollection[i].Balance = parking.VehicleCollection[i].Balance - CheckFineAndReturnPrice(parking.VehicleCollection[i], Settings.PassengerCar);
                            parking.balance = parking.balance + CheckFineAndReturnPrice(parking.VehicleCollection[i], Settings.PassengerCar);
                            transactionInfo.Add(new TransactionInfo(e?.SignalTime.ToString(), parking.VehicleCollection[i].Id, Settings.PassengerCar));
                            break;
                        }
                    case VehicleType.Truck:
                        {
                            parking.VehicleCollection[i].Balance = parking.VehicleCollection[i].Balance - CheckFineAndReturnPrice(parking.VehicleCollection[i], Settings.Truck);
                            parking.balance = parking.balance + CheckFineAndReturnPrice(parking.VehicleCollection[i], Settings.Truck);
                            transactionInfo.Add(new TransactionInfo(e?.SignalTime.ToString(), parking.VehicleCollection[i].Id, Settings.Truck));
                            break;
                        }
                    case VehicleType.Bus:
                        {
                            parking.VehicleCollection[i].Balance = parking.VehicleCollection[i].Balance - CheckFineAndReturnPrice(parking.VehicleCollection[i], Settings.Bus);
                            parking.balance = parking.balance + CheckFineAndReturnPrice(parking.VehicleCollection[i], Settings.Bus);
                            transactionInfo.Add(new TransactionInfo(e?.SignalTime.ToString(), parking.VehicleCollection[i].Id, Settings.Bus));
                            break;
                        }
                    case VehicleType.Motorcycle:
                        {
                            parking.VehicleCollection[i].Balance = parking.VehicleCollection[i].Balance - CheckFineAndReturnPrice(parking.VehicleCollection[i], Settings.Motorcycle);
                            parking.balance = parking.balance + CheckFineAndReturnPrice(parking.VehicleCollection[i], Settings.Motorcycle);
                            transactionInfo.Add(new TransactionInfo(e?.SignalTime.ToString(), parking.VehicleCollection[i].Id, Settings.Motorcycle));
                            break;
                        }
                }
            }           
        }

        private decimal CheckFineAndReturnPrice(Vehicle v, decimal payment)
        {
            decimal fine;
            if (v.Balance - payment < 0 && v.Balance > 0)
            {
                fine = -(v.Balance - payment) * Settings.fine;
                fine += payment - v.Balance;
            }
            else if (v.Balance <= 0)
            {
                fine = payment * Settings.fine;
            }
            else
            {
                fine = payment;
            }
            return fine;
        }

        private void Write(object sender, ElapsedEventArgs e)
        {
            StringBuilder infoLog = new StringBuilder("");
            for (int i = 0; i < transactionInfo.Count; i++)
            {
                infoLog.Append(transactionInfo[i].ToString() + "\n");
            }
            logService.Write(infoLog.ToString());
            transactionInfo.Clear();
        }


        public string ReadFromLog()
        {
            return logService.Read();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactionInfo.ToArray();
        }

        public decimal GetBalance()
        {
            return parking.balance;
        }

        public int GetCapacity()
        {
            return Settings.capacity;
        }

        public int GetFreePlaces()
        {
            return Settings.capacity - parking.VehicleCollection.Count;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(parking.VehicleCollection);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (this.GetFreePlaces() > 0)
            {
                if (parking.VehicleCollection.Find((Vehicle x) => x.Id == vehicle.Id) == null)
                {
                    parking.VehicleCollection.Add(vehicle);
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            Vehicle temp = parking.VehicleCollection.Find((Vehicle x) => x.Id == vehicleId);
            if (temp != null)
            {
                if (temp.Balance > 0)
                {
                    parking.VehicleCollection.Remove(temp);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            int index = parking.VehicleCollection.FindIndex((Vehicle x) => x.Id == vehicleId);
            if (index != -1)
            {
                if (sum > 0)
                {
                    parking.VehicleCollection[index].Balance = parking.VehicleCollection[index].Balance + sum;
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            else
            {
                throw new ArgumentException();
            }
        }



        public void Dispose()
        {
            parking.VehicleCollection = new List<Vehicle>();
            parking.balance = Settings.initialBalance;
            transactionInfo = new List<TransactionInfo>();
        }

    }
}