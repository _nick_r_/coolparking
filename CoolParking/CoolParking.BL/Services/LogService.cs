﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.


using CoolParking.BL.Interfaces;
using System;
using System.IO;


namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath { get; }

        public LogService(string path)
        {
            LogPath = path;
        }

        public void Write(string logInfo)
        {
            FileInfo fileInfo = new FileInfo(LogPath);
            if (fileInfo.Exists)
            {
                using (StreamWriter sw = File.AppendText(LogPath))
                {
                    sw.Write(logInfo + "\n");
                }
            }
            else
            {
                using (StreamWriter sw = File.CreateText(LogPath))
                {
                    sw.Write(logInfo + "\n");
                }
            }

        }

        public string Read()
        {
            FileInfo fileInfo = new FileInfo(LogPath);
            string result = "";
            if (fileInfo.Exists)
            {
                using (StreamReader sr = new StreamReader(LogPath))
                {
                    result += sr.ReadToEnd();
                }
            }
            else
            {
                throw new InvalidOperationException();
            }

            return result;
        }


    }

}